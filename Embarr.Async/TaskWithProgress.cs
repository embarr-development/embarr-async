﻿using System.Threading.Tasks;
using Embarr.Async.Models;

namespace Embarr.Async
{
    public class TaskWithProgress
    {
        public Task Task { get; set; }
        public ProgressConfiguration ProgressConfiguration { get; set; }
        public decimal PercentageOfTotal { get; set; }
    }
}