﻿namespace Embarr.Async.Models
{
    public class ProgressReport
    {
        public decimal PercentageComplete { get; set; }
        public decimal PercentageOfTotal { get; set; }
        public string Message { get; set; }
    }
}