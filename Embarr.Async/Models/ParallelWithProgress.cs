﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Embarr.Async.Contracts;
using Embarr.Async.DbC;

namespace Embarr.Async.Models
{
    /// <summary>
    /// Class for running multiple actions asynchronously using interleaving to report progress information.
    /// </summary>
    public class ParallelWithProgress : IProgressAction, IParallelWithProgress
    {
        private readonly ParallelWithProgressConfiguration parallelWithProgressConfiguration;
        private readonly CancellationTokenSource cancellationTokenSource;
        private Action<ProgressReport> progressReport;
        private readonly TaskFactory taskFactory;
        private readonly List<TaskWithProgress> taskWithProgressList;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParallelWithProgress"/> class.
        /// </summary>
        /// <param name="parallelWithProgressConfiguration">The configuration.</param>
        /// <param name="cancellationTokenSource">The CancellationTokenSource. If not passed in an internal instance is created for use with the Cancel method.</param>
        protected ParallelWithProgress(ParallelWithProgressConfiguration parallelWithProgressConfiguration = null, CancellationTokenSource cancellationTokenSource = null)
        {
            this.parallelWithProgressConfiguration = parallelWithProgressConfiguration ?? new ParallelWithProgressConfiguration();
            this.cancellationTokenSource = cancellationTokenSource ?? new CancellationTokenSource();
            taskFactory = new TaskFactory(this.cancellationTokenSource.Token);
            taskWithProgressList = new List<TaskWithProgress>();
        }

        /// <summary>
        /// Configures the instance.
        /// </summary>
        /// <param name="parallelWithProgressConfiguration">The configuration.</param>
        /// <param name="cancellationTokenSource">The CancellationTokenSource.</param>
        /// <returns></returns>
        public static IProgressAction Configure(
            ParallelWithProgressConfiguration parallelWithProgressConfiguration = null, 
            CancellationTokenSource cancellationTokenSource = null)
        {
            return new ParallelWithProgress(parallelWithProgressConfiguration, cancellationTokenSource);
        }

        /// <summary>
        /// Adds a custom action which will be called each time a task or action completes.
        /// </summary>
        /// <param name="progressReportAction">The progress report action.</param>
        /// <returns></returns>
        public IParallelWithProgress AddProgressAction(Action<ProgressReport> progressReportAction)
        {
            Requires.NotNull(() => progressReportAction, progressReportAction);

            progressReport = progressReportAction;

            return this;
        }

        /// <summary>
        /// Adds an action that will be run asynchronously.
        /// </summary>
        /// <param name="asyncAction">The asynchronous action.</param>
        /// <param name="weight">You can specify a weight to alter the percentage this action is worth against the overall set of tasks and actions (must be zero or above).</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public IParallelWithProgress AddAction(Action asyncAction, int weight = 1, string message = null)
        {
            Requires.NotNull(() => asyncAction, asyncAction);
            Requires.GreaterThanOrEqualTo(() => weight, 0, weight);

            taskWithProgressList.Add(CreateTaskWithProgress(taskFactory.StartNew(asyncAction), weight, message));

            return this;
        }

        /// <summary>
        /// Adds the specified task.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <param name="weight">You can specify a weight to alter the percentage this action is worth against the overall set of tasks and actions (must be zero or above).</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public IParallelWithProgress AddTask(Task task, int weight = 1, string message = null)
        {
            Requires.NotNull(() => task, task);
            Requires.GreaterThanOrEqualTo(() => weight, 0, weight);

            taskWithProgressList.Add(CreateTaskWithProgress(task, weight, message));

            return this;
        }

        /// <summary>
        /// Starts the reporter.
        /// </summary>
        /// <returns></returns>
        public async Task StartReporting()
        {
            SetPercentages();

            decimal percentageComplete = 0;

            while (taskWithProgressList.Count > 0)
            {
                var task = await Task.WhenAny(taskWithProgressList.Select(x => x.Task));
                var taskWithProgress = taskWithProgressList.First(x => x.Task == task);

                percentageComplete += taskWithProgress.PercentageOfTotal;

                progressReport(new ProgressReport
                {
                    Message = taskWithProgress.ProgressConfiguration.Message,
                    PercentageComplete = taskWithProgressList.Count == 1 ? 100 : percentageComplete,
                    PercentageOfTotal = taskWithProgress.PercentageOfTotal
                });

                taskWithProgressList.Remove(taskWithProgress);
            } 
        }

        /// <summary>
        /// Calls cancel on the CancellationTokenSource.
        /// </summary>
        public void Cancel()
        {
            cancellationTokenSource.Cancel();
        }

        private static TaskWithProgress CreateTaskWithProgress(Task task, int weight, string message)
        {
            return new TaskWithProgress
            {
                Task = task,
                ProgressConfiguration = new ProgressConfiguration
                {
                    Message = message,
                    Weight = weight
                }
            };
        }
        
        private void SetPercentages()
        {
            if (AllTasksAreUsingDefaultWeighting())
            {
                SetSimplePercentageOfTotal();
            }
            else
            {
                SetWeightedPercentageOfTotal();
            }
        }

        private void SetWeightedPercentageOfTotal()
        {
            var totalWeights = taskWithProgressList.Select(x => x.ProgressConfiguration.Weight).Sum();
            var individualPercentagePoint = (decimal) 100/totalWeights;
            foreach (var taskWithProgress in taskWithProgressList)
            {
                taskWithProgress.PercentageOfTotal = CalculateWeightedPercentage(taskWithProgress.ProgressConfiguration.Weight,
                    individualPercentagePoint);
            }
        }

        private void SetSimplePercentageOfTotal()
        {
            var simplePercentage = CalculateSimplePercentage();

            foreach (var taskWithProgress in taskWithProgressList)
            {
                taskWithProgress.PercentageOfTotal = simplePercentage;
            }
        }

        private decimal CalculateWeightedPercentage(int weight, decimal individualPercentagePoint)
        {
            return Math.Round(weight*individualPercentagePoint, parallelWithProgressConfiguration.DecimalPlaces);
        }

        private decimal CalculateSimplePercentage()
        {
            return Math.Round((decimal) 100/taskWithProgressList.Count, parallelWithProgressConfiguration.DecimalPlaces);
        }

        private bool AllTasksAreUsingDefaultWeighting()
        {
            return taskWithProgressList.All(x => x.ProgressConfiguration.Weight == 1);
        }
    }
}