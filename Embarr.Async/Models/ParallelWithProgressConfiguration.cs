﻿namespace Embarr.Async.Models
{
    public class ParallelWithProgressConfiguration
    {
        public int DecimalPlaces { get; set; }

        public ParallelWithProgressConfiguration()
        {
            DecimalPlaces = 2;
        }
    }
}