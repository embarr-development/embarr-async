using System;

namespace Embarr.Async.Models
{
    public class ProgressConfiguration
    {
        public string Message { get; set; }
        public int Weight { get; set; }
    }
}