using System;
using Embarr.Async.Models;

namespace Embarr.Async.Contracts
{
    public interface IProgressAction
    {
        /// <summary>
        /// Adds a custom action which will be called each time a task or action completes.
        /// </summary>
        /// <param name="progressReportAction">The progress report action.</param>
        /// <returns></returns>
        IParallelWithProgress AddProgressAction(Action<ProgressReport> progressReportAction);
    }
}