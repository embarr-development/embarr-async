﻿using System;
using System.Threading.Tasks;

namespace Embarr.Async.Contracts
{
    public interface IParallelWithProgress
    {
        /// <summary>
        /// Adds an action that will be run asynchronously.
        /// </summary>
        /// <param name="asyncAction">The asynchronous action.</param>
        /// <param name="weight">You can specify a weight to alter the percentage this action is worth against the overall set of tasks and actions (must be zero or above).</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        IParallelWithProgress AddAction(Action asyncAction, int weight = 1, string message = null);

        /// <summary>
        /// Adds the specified task.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <param name="weight">You can specify a weight to alter the percentage this action is worth against the overall set of tasks and actions (must be zero or above).</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        IParallelWithProgress AddTask(Task task, int weight = 1, string message = null);

        /// <summary>
        /// Starts the reporter.
        /// </summary>
        /// <returns></returns>
        Task StartReporting();

        /// <summary>
        /// Calls cancel on the CancellationTokenSource.
        /// </summary>
        void Cancel();
    }
}