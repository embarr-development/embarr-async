﻿using System;
using System.Linq.Expressions;

namespace Embarr.Async.DbC
{
    public class Requires
    {
        public static void NotNull<T>(Expression<Func<T>> parameter, T value)
        {
            if ((object)value == null)
            {
                throw new ArgumentNullException(GetParameterName(parameter));
            }
        }

        public static void GreaterThanOrEqualTo<T>(Expression<Func<T>> parameter, int minValue, int value)
        {
            if (value < minValue)
            {
                throw new ArgumentException(string.Format("The {0} cannot be less than {1}", GetParameterName(parameter), minValue));
            }
        }

        private static string GetParameterName(LambdaExpression reference)
        {
            return ((MemberExpression)reference.Body).Member.Name;
        }
    }
}