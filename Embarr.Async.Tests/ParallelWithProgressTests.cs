﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Embarr.Async.Models;
using NUnit.Framework;
using Should;

namespace Embarr.Async.Tests
{
    [TestFixture]
    public class ParallelWithProgressTests
    {
        public class AddProgressAction : ParallelWithProgressTests
        {
            [Test]
            public void Should_Throw_Exception_If_Action_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentNullException>(() => ParallelWithProgress.Configure().AddProgressAction(null));

                // Assert
                exception.ShouldNotBeNull();
                exception.Message.ShouldContain("Parameter name: progressReportAction");
            }

            [Test]
            public void Should_Return_Instance()
            {
                // Act
                var instance = ParallelWithProgress.Configure().AddProgressAction(p => { });

                // Assert
                instance.ShouldNotBeNull();
            }
        }

        public class AddAction : ParallelWithProgressTests
        {
            [Test]
            public void Should_Throw_Exception_If_AsyncAction_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentNullException>(() => ParallelWithProgress.Configure().AddProgressAction(
                    p => { })
                    .AddAction(null));

                // Assert
                exception.ShouldNotBeNull();
                exception.Message.ShouldContain("Parameter name: asyncAction");
            }

            [Test]
            public void Should_Throw_Exception_If_Weight_Is_Less_Than_Zero()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => ParallelWithProgress.Configure().AddProgressAction(
                    p => { })
                    .AddAction(() => { }, -1));

                // Assert
                exception.ShouldNotBeNull();
                exception.Message.ShouldContain("The weight cannot be less than 0");
            }
        }

        public class AddTask : ParallelWithProgressTests
        {
            [Test]
            public void Should_Throw_Exception_If_Task_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentNullException>(() => ParallelWithProgress.Configure().AddProgressAction(
                    p => { })
                    .AddTask(null));

                // Assert
                exception.ShouldNotBeNull();
                exception.Message.ShouldContain("Parameter name: task");
            }

            [Test]
            public void Should_Throw_Exception_If_Weight_Is_Less_Than_Zero_For_Task_Add_Method()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => ParallelWithProgress.Configure().AddProgressAction(
                    p => { })
                    .AddTask(new Task(() => { }), -1));

                // Assert
                exception.ShouldNotBeNull();
                exception.Message.ShouldContain("The weight cannot be less than 0");
            }
        }

        public class StartReporting : ParallelWithProgressTests
        {
            [Test]
            public async Task Should_Report_Simple_Progress_With_Equal_Percentages_Per_Action()
            {
                // Arrange
                var individualPercentages = new List<decimal>();
                decimal finalPercentage = 0;

                Action<ProgressReport> reporter = p =>
                {
                    individualPercentages.Add(p.PercentageOfTotal);
                    finalPercentage = p.PercentageComplete;
                };

                // Act
                await ParallelWithProgress.Configure()
                    .AddProgressAction(reporter)
                    .AddAction(LongRunningMethod)
                    .AddAction(LongRunningMethod)
                    .AddAction(LongRunningMethod)
                    .AddAction(LongRunningMethod)
                    .StartReporting();

                // Assert
                finalPercentage.ShouldEqual(100);
                individualPercentages.All(x => x == 25).ShouldBeTrue();
            }

            [Test]
            public async Task Should_Report_Simple_Progress_For_Odd_Number_Of_Actions_With_Equal_Percentages_Per_Action()
            {
                // Arrange
                var individualPercentages = new List<decimal>();
                decimal finalPercentage = 0;

                Action<ProgressReport> reporter = p =>
                {
                    individualPercentages.Add(p.PercentageOfTotal);
                    finalPercentage = p.PercentageComplete;
                };

                // Act
                await ParallelWithProgress.Configure()
                    .AddProgressAction(reporter)
                    .AddAction(LongRunningMethod)
                    .AddAction(LongRunningMethod)
                    .AddAction(LongRunningMethod)
                    .StartReporting();

                // Assert
                finalPercentage.ShouldEqual(100);
                individualPercentages.All(x => x == 33.33M).ShouldBeTrue();
            }

            [Test]
            public async Task Should_Report_Messages_For_Each_Action()
            {
                // Arrange
                var individualMessages = new List<string>();
                decimal finalPercentage = 0;

                Action<ProgressReport> reporter = p =>
                {
                    individualMessages.Add(p.Message);
                    finalPercentage = p.PercentageComplete;
                };

                // Act
                await ParallelWithProgress.Configure()
                    .AddProgressAction(reporter)
                    .AddAction(LongRunningMethod, message: "Message 1")
                    .AddAction(LongRunningMethod, message: "Message 2")
                    .AddAction(LongRunningMethod, message: "Message 3")
                    .AddAction(LongRunningMethod, message: "Message 4")
                    .StartReporting();

                // Assert
                finalPercentage.ShouldEqual(100);
                individualMessages.ShouldContain("Message 1");
                individualMessages.ShouldContain("Message 2");
                individualMessages.ShouldContain("Message 3");
                individualMessages.ShouldContain("Message 4");
            }

            [Test]
            public async Task Should_Alter_Percentages_Based_On_Weight_For_Actions()
            {
                // Arrange
                var progressReports = new List<decimal>();

                Action<ProgressReport> reporter = p => progressReports.Add(p.PercentageComplete);

                // Act
                await ParallelWithProgress.Configure()
                    .AddProgressAction(reporter)
                    .AddAction(LongRunningMethod, 80)
                    .AddAction(LongRunningMethod, 10)
                    .AddAction(LongRunningMethod, 5)
                    .AddAction(LongRunningMethod, 5)
                    .StartReporting();

                // Assert
                progressReports.Count.ShouldEqual(4);
                progressReports.First().ShouldNotEqual(25);
            }

            [Test]
            public async Task Should_Report_Simple_Progress_With_Equal_Percentages_Per_Task()
            {
                // Arrange
                var individualPercentages = new List<decimal>();
                decimal finalPercentage = 0;

                Action<ProgressReport> reporter = p =>
                {
                    individualPercentages.Add(p.PercentageOfTotal);
                    finalPercentage = p.PercentageComplete;
                };

                var taskFactory = new TaskFactory();

                // Act
                await ParallelWithProgress.Configure()
                    .AddProgressAction(reporter)
                    .AddTask(taskFactory.StartNew(LongRunningMethod))
                    .AddTask(taskFactory.StartNew(LongRunningMethod))
                    .AddTask(taskFactory.StartNew(LongRunningMethod))
                    .AddTask(taskFactory.StartNew(LongRunningMethod))
                    .StartReporting();

                // Assert
                finalPercentage.ShouldEqual(100);
                individualPercentages.All(x => x == 25).ShouldBeTrue();
            }

            [Test]
            public async Task Should_Report_Simple_Progress_For_Odd_Number_Of_Tasks_With_Equal_Percentages_Per_Task()
            {
                // Arrange
                var individualPercentages = new List<decimal>();
                decimal finalPercentage = 0;

                Action<ProgressReport> reporter = p =>
                {
                    individualPercentages.Add(p.PercentageOfTotal);
                    finalPercentage = p.PercentageComplete;
                };

                var taskFactory = new TaskFactory();

                // Act
                await ParallelWithProgress.Configure()
                    .AddProgressAction(reporter)
                    .AddTask(taskFactory.StartNew(LongRunningMethod))
                    .AddTask(taskFactory.StartNew(LongRunningMethod))
                    .AddTask(taskFactory.StartNew(LongRunningMethod))
                    .StartReporting();

                // Assert
                finalPercentage.ShouldEqual(100);
                individualPercentages.All(x => x == 33.33M).ShouldBeTrue();
            }

            [Test]
            public async Task Should_Report_Messages_For_Each_Task()
            {
                // Arrange
                var individualMessages = new List<string>();
                decimal finalPercentage = 0;

                Action<ProgressReport> reporter = p =>
                {
                    individualMessages.Add(p.Message);
                    finalPercentage = p.PercentageComplete;
                };

                var taskFactory = new TaskFactory();

                // Act
                await ParallelWithProgress.Configure()
                    .AddProgressAction(reporter)
                    .AddTask(taskFactory.StartNew(LongRunningMethod), message: "Message 1")
                    .AddTask(taskFactory.StartNew(LongRunningMethod), message: "Message 2")
                    .AddTask(taskFactory.StartNew(LongRunningMethod), message: "Message 3")
                    .AddTask(taskFactory.StartNew(LongRunningMethod), message: "Message 4")
                    .StartReporting();

                // Assert
                finalPercentage.ShouldEqual(100);
                individualMessages.ShouldContain("Message 1");
                individualMessages.ShouldContain("Message 2");
                individualMessages.ShouldContain("Message 3");
                individualMessages.ShouldContain("Message 4");
            }

            [Test]
            public async Task Should_Alter_Percentages_Based_On_Weight_For_Tasks()
            {
                // Arrange
                var progressReports = new List<decimal>();

                Action<ProgressReport> reporter = p => progressReports.Add(p.PercentageComplete);

                var taskFactory = new TaskFactory();

                // Act
                await ParallelWithProgress.Configure()
                    .AddProgressAction(reporter)
                    .AddTask(taskFactory.StartNew(LongRunningMethod), 80)
                    .AddTask(taskFactory.StartNew(LongRunningMethod), 10)
                    .AddTask(taskFactory.StartNew(LongRunningMethod), 5)
                    .AddTask(taskFactory.StartNew(LongRunningMethod), 5)
                    .StartReporting();

                // Assert
                progressReports.Count.ShouldEqual(4);
                progressReports.First().ShouldNotEqual(25);
            }
        }

        public class Integration : ParallelWithProgressTests
        {
            [Test]
            public async Task Should_Report_Messages_For_Each_Action_In_The_Test_Console()
            {
                // Arrange
                Action<ProgressReport> reporter = p => Console.WriteLine("Progress:{0}% - {1}", p.PercentageComplete, p.Message);

                // Act
                await ParallelWithProgress.Configure()
                    .AddProgressAction(reporter)
                    .AddAction(LongRunningMethod, message: "Message 1")
                    .AddAction(LongRunningMethod, message: "Message 2")
                    .AddAction(LongRunningMethod, message: "Message 3")
                    .AddAction(LongRunningMethod, message: "Message 4")
                    .AddAction(LongRunningMethod, message: "Message 5")
                    .AddAction(LongRunningMethod, message: "Message 6")
                    .AddAction(LongRunningMethod, message: "Message 7")
                    .AddAction(LongRunningMethod, message: "Message 8")
                    .AddAction(LongRunningMethod, weight:20, message: "Message 9")
                    .AddAction(LongRunningMethod, message: "Message 10")
                    .AddAction(LongRunningMethod, message: "Message 11")
                    .AddAction(LongRunningMethod, message: "Message 12")
                    .AddAction(LongRunningMethod, message: "Message 13")
                    .AddAction(LongRunningMethod, message: "Message 14")
                    .AddAction(LongRunningMethod, message: "Message 15")
                    .AddAction(LongRunningMethod, message: "Message 16")
                    .AddAction(LongRunningMethod, message: "Message 17")
                    .StartReporting();
            }
        }

        private void LongRunningMethod()
        {
            Thread.Sleep(TimeSpan.FromMilliseconds(250));
        }
    }
}