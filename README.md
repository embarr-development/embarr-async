# Embarr Async #

At the moment it has one main helper called ParallelWithProgress.
You can add any number of tasks or actions which will run asynchronously and call a custom action with a progress report each time a task/action completes.


### Simple Examples ###

```
#!c#

await ParallelWithProgress.Configure()
    .AddProgressAction(progress => Console.WriteLine("Progress:{0}% - {1}", p.PercentageComplete, p.Message))
    .AddAction(LongRunningMethod)
    .AddAction(LongRunningMethod)
    .AddAction(LongRunningMethod)
    .AddAction(LongRunningMethod)
    .StartReporting();
```
```
#!c#
await ParallelWithProgress.Configure()
    .AddProgressAction(progress => Console.WriteLine("Progress:{0}% - {1}", p.PercentageComplete, p.Message))
    .AddTask(taskFactory.StartNew(LongRunningMethod))
    .AddTask(taskFactory.StartNew(LongRunningMethod))
    .AddTask(taskFactory.StartNew(LongRunningMethod))
    .AddTask(taskFactory.StartNew(LongRunningMethod))
    .StartReporting();
```

### Add Weights and Messages ###

Messages simply report a custom message with each task/action completion.
Weighting allows you to take control over how much each task/action is worth as part of the total percentage.
Weights cannot be negative numbers.

```
#!c#

await ParallelWithProgress.Configure()
    .AddProgressAction(progress => Console.WriteLine("Progress:{0}% - {1}", p.PercentageComplete, p.Message))
    .AddAction(LongRunningMethod, weight:10, message: "This is message 1")
    .AddAction(LongRunningMethod, weight:8, message: "This is message 2")
    .AddAction(LongRunningMethod, weight:6, message: "This is message 3")
    .AddAction(LongRunningMethod, weight:4, message: "This is message 4")
    .StartReporting();
```

### Configuration ###
You can alter the number of decimal points the percentages return.

### Cancellation ###
You can either pass in a CancellationTokenSource or the helper will generate one for you.
This CancellationTokenSource is used in the .Cancel() method.

```
#!c#

 var parallelWithProgress = ParallelWithProgress.Configure()
      .AddProgressAction(progress => Console.WriteLine("Progress:{0}% - {1}", p.PercentageComplete, p.Message))
      .AddAction(LongRunningMethod)
      .AddAction(LongRunningMethod)
      .AddAction(LongRunningMethod)
      .AddAction(LongRunningMethod);

  await parallelWithProgress.StartReporting();

  parallelWithProgress.Cancel();
```
### Progress Report ###
The progress report is the following class populated:

```
#!c#
 public class ProgressReport
  {
      public decimal PercentageComplete { get; set; }
      public decimal PercentageOfTotal { get; set; }
      public string Message { get; set; }
  }
```
PercentageComplete is the overall percentage completion across all tasks/actions.

PercentageOfTotal is the individual percentage the completed task/action represents.

Message is a custom message specified to be included in the report when the specified task/action completes.